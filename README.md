## About

server.rb is a static file web server implemented with
[puma](https://github.com/puma/puma)
and
[rack](https://github.com/rack/rack).

## Example

**Server.for**

The
[`Server.for`](http://0x1eef.github.io/x/server.rb/Server.html#for-class_method)
method receives the path to a directory, and returns
an instance of
[Server](http://0x1eef.github.io/x/server.rb/Server.html).
The
[Server#start](http://0x1eef.github.io/x/server.rb/Server.html#start-instance_method)
method can spawn a HTTP server on a separate thread, and the thread can
be blocking or non-blocking:

```ruby
require "server"
server = Server.for File.join(Dir.getwd, "website")
server.start(block: true)
```

**Options**

[`Server.for`](http://0x1eef.github.io/x/server.rb/Server.html#for-class_method)
accepts options that are forwarded onto the Puma web server. The `unix`,
`host`, and `port` options are supported by server.rb and translated
to a matching  `binds` option that Puma natively supports:

```ruby
require "server"
server = Server.for File.join(Dir.getwd, "website"),
                    unix: File.join(Dir.getwd, "website.sock")
```

## Documentation

A complete API reference is available at
[0x1eef.github.io/x/server.rb](https://0x1eef.github.io/x/server.rb)

## Install

server.rb can be installed via rubygems.org:

``` ruby
gem install server.rb
```

## Sources

* [Github](https://github.com/0x1eef/server.rb#readme)
* [Gitlab](https://gitlab.com/0x1eef/server.rb#about)

## License

[BSD Zero Clause](https://choosealicense.com/licenses/0bsd/)
<br>
See [LICENSE](./LICENSE)
