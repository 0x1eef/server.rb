# frozen_string_literal: true

##
# {Server::IndexResolver Server::IndexResolver} is a
# middleware that serves the contents of index.html
# from a given directory
class Server::IndexResolver
  def initialize(app, options = {})
    @app = app
    @root = options.fetch(:root)
  end

  def call(env)
    index = File.join(root, File.expand_path(env["PATH_INFO"]), "index.html")
    if File.exist?(index)
      Rack::Response[
        200,
        {"content-type" => "text/html"},
        File.read(index).each_line
      ].finish
    else
      @app.call(env)
    end
  end

  private
  attr_reader :root
end
